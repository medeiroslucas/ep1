# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

* Inserir código do candidato
* Confirmar voto
* Voto Branco
* Voto Nulo
* Corrigir voto
* Apresentar resultado parcial

## Bugs e problemas

* Escrever caracteres ao ser solicitado o número de eleitores

## Referências

[Repositório de dados eleitorais](http://www.tse.jus.br/eleicoes/estatisticas/repositorio-de-dados-eleitorais-1/)