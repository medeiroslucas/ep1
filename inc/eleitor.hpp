#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include<bits/stdc++.h>

using namespace std;

class Eleitor{

    private:

        string NOME;
        string VOTO_DEPUTADO_FEDERAL;
        string VOTO_DEPUTADO_DISTRITAL;
        string VOTO_SENADOR_1;
        string VOTO_SENADOR_2;
        string VOTO_GOVERNADOR;
        string VOTO_PRESIDENTE;

    public:

        Eleitor();
        ~Eleitor();

        string get_NOME();
        void set_NOME(string NOME);


        string get_VOTO_DEPUTADO_FEDERAL();
        void set_VOTO_DEPUTADO_FEDERAL(string VOTO_DEPUTADO_FEDERAL);

        string get_VOTO_DEPUTADO_DISTRITAL();
        void set_VOTO_DEPUTADO_DISTRITAL(string VOTO_DEPUTADO_DISTRITAL);

        string get_VOTO_SENADOR_1();
        void set_VOTO_SENADOR_1(string VOTO_SENADOR_1);

        string get_VOTO_SENADOR_2();
        void set_VOTO_SENADOR_2(string VOTO_SENADOR_2);

        string get_VOTO_GOVERNADOR();
        void set_VOTO_GOVERNADOR(string VOTO_GOVERNADOR);

        string get_VOTO_PRESIDENTE();
        void set_VOTO_PRESIDENTE(string VOTO_PRESIDENTE);

        void print_ELEITOR();

};



#endif