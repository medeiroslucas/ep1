#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <bits/stdc++.h>

using namespace std;

class Candidato{
private:

    int NR_VOTOS;
    string NR_CANDIDATO;
    string CD_CARGO;
    string NM_UE; 
    string DS_CARGO;
    //string SQ_CANDIDATO;
    string NM_CANDIDATO;
    string NM_URNA_CANDIDATO;
    string CD_SITUACAO_CANDIDATURA;
    string NR_PARTIDO;
    string SG_PARTIDO;
    string NM_PARTIDO;

public:

    Candidato();
    ~Candidato();

    int get_NR_VOTOS();
    void set_NR_VOTOS(int NR_VOTOS);

    string get_NR_CANDIDATO();
    void set_NR_CANDIDATO(string NR_CANDIDATO);

    string get_CD_CARGO();
    void set_CD_CARGO(string CD_CARGO);

    string get_NM_UE();
    void set_NM_UE(string NM_UE);
    
    string get_DS_CARGO();
    void set_DS_CARGO(string DS_CARGO);

    /*string get_SQ_CANDIDATO();
    void set_SQ_CANDIDATO(string SQ_CANDIDATO);*/

    string get_NM_CANDIDATO();
    void set_NM_CANDIDATO(string NM_CANDIDATO);

    string get_NM_URNA_CANDIDATO();
    void set_NM_URNA_CANDIDATO(string NM_URNA_CANDIDATO);

    string get_CD_SITUACAO_CANDIDATURA();
    void set_CD_SITUACAO_CANDIDATURA(string CD_SITUACAO_CANDIDATURA);

    string get_NR_PARTIDO();
    void set_NR_PARTIDO(string NR_PARTIDO);

    string get_SG_PARTIDO();
    void set_SG_PARTIDO(string SG_PARTIDO);

    string get_NM_PARTIDO();
    void set_NM_PARTIDO(string NM_PARTIDO);

    void contar_voto();

    void print_candidato();

    void print_urna();


};

#endif
