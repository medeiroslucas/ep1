#include <bits/stdc++.h>

using namespace std;

#include "candidato.hpp"

Candidato::Candidato(){
}

Candidato::~Candidato(){
}

int Candidato::get_NR_VOTOS(){
    return NR_VOTOS;
}
void Candidato::set_NR_VOTOS(int NR_VOTOS){
    this->NR_VOTOS = NR_VOTOS;
}

string Candidato::get_NR_CANDIDATO(){
    return NR_CANDIDATO;
}
void Candidato::set_NR_CANDIDATO(string NR_CANDIDATO){
    this->NR_CANDIDATO = NR_CANDIDATO;
}

string Candidato::get_CD_CARGO(){
    return CD_CARGO;
}
void Candidato::set_CD_CARGO(string CD_CARGO){
    this->CD_CARGO = CD_CARGO;
}

string Candidato::get_NM_UE(){
    return NM_UE;
}
void Candidato::set_NM_UE(string NM_UE){
    this->NM_UE = NM_UE;
}

string Candidato::get_DS_CARGO(){
    return DS_CARGO;
}
void Candidato::set_DS_CARGO(string DS_CARGO){
    this->DS_CARGO = DS_CARGO;
}

/*string Candidato::get_SQ_CANDIDATO(){
    return SQ_CANDIDATO;
}
void Candidato::set_SQ_CANDIDATO(string SQ_CANDIDATO){
    this->SQ_CANDIDATO = SQ_CANDIDATO;
}*/

string Candidato::get_NM_CANDIDATO(){
    return NM_CANDIDATO;
}
void Candidato::set_NM_CANDIDATO(string NM_CANDIDATO){
    this->NM_CANDIDATO = NM_CANDIDATO;
}

string Candidato::get_NM_URNA_CANDIDATO(){
    return NM_URNA_CANDIDATO;
}
void Candidato::set_NM_URNA_CANDIDATO(string NM_URNA_CANDIDATO){
    this->NM_URNA_CANDIDATO = NM_URNA_CANDIDATO; 
}

string Candidato::get_CD_SITUACAO_CANDIDATURA(){
    return CD_SITUACAO_CANDIDATURA;
}
void Candidato::set_CD_SITUACAO_CANDIDATURA(string CD_SITUACAO_CANDIDATURA){
    this->CD_SITUACAO_CANDIDATURA = CD_SITUACAO_CANDIDATURA; 
}

string Candidato::get_NR_PARTIDO(){
    return NR_PARTIDO;
}
void Candidato::set_NR_PARTIDO(string NR_PARTIDO){
    this->NR_PARTIDO = NR_PARTIDO;
}

string Candidato::get_SG_PARTIDO(){
    return SG_PARTIDO;
}
void Candidato::set_SG_PARTIDO(string SG_PARTIDO){
    this->SG_PARTIDO = SG_PARTIDO;
}

string Candidato::get_NM_PARTIDO(){
    return NM_PARTIDO;
}
void Candidato::set_NM_PARTIDO(string NM_PARTIDO){
    this->NM_PARTIDO = NM_PARTIDO;
}

            //NM_UE;CD_CARGO;DS_CARGO;NR_CANDIDATO;NM_CANDIDATO;NM_URNA_CANDIDATO;NR_PARTIDO;SG_PARTIDO;NM_PARTIDO

void Candidato::contar_voto(){
    NR_VOTOS += 1;
}

void Candidato::print_candidato(){
    cout << NM_UE << endl;
    cout << CD_CARGO << endl;
    cout << DS_CARGO << endl;
    cout << NR_CANDIDATO << endl;
    cout << NM_CANDIDATO << endl;
    cout << NM_URNA_CANDIDATO << endl;
    cout << NR_PARTIDO << endl;
    cout << SG_PARTIDO << endl;
    cout << NM_PARTIDO << endl;

}

void Candidato::print_urna(){

    cout << NM_URNA_CANDIDATO << endl;
    cout << NM_PARTIDO << endl;

}