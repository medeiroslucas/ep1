#include <bits/stdc++.h>

#include "candidato.hpp"
#include "eleitor.hpp"
#include "main.hpp"

using namespace std;

string nesima_palavra(int n, char *linha){

    int contpv=0, i;
    string nesima_palavra = "";
    string erro = "Erro";

    for(i=0; linha[i] != '\0' ; i++){

        if(contpv == n-1){

            while( (linha[i] != ';') && (linha[i] != '\0') ){
                nesima_palavra.push_back(linha[i]);
                i++;
            }

            return nesima_palavra; 
   
        }

        if(*(linha+i) == ';')contpv++;
    }

    return erro;
}

string upper(string a){
    for(unsigned int k=0; k < a.size(); k++){
        a[k] = toupper(a[k]);
    }
    return a;
}

int main() {

    Candidato candidato[1266];
    vector<Eleitor> eleitores;
    Eleitor eleitor;
    string nome = "";
    string voto = "";
    string confirmacao = "";
    bool inelegivel = false, voto_senador_repetido = false, voto_valido=false;

    char *linha;

    int a, b, c, i= -1, tamanho, j, votos_brancos=0, votos_nulos=0, numero_eleitores, indice_candidato=-1;

    /* ####################################
        Aquisiçãos dos dados federais
    #####################################*/

    FILE *f = fopen("data/consulta_cand_2018_BR.csv", "r");

    for(; i < 26; i++){

        a = ftell(f);

        while((c = fgetc(f) != '\n'));

        b = ftell(f);

        tamanho = b-a;

        linha = (char *) malloc(tamanho*sizeof(char));
        
        fseek(f, a, SEEK_SET);

        fgets(linha, tamanho, f);

        fseek(f, b, SEEK_SET);

        if(i != -1){
           // preencher_candidato(&(candidato[i]), linha);

            //cout << candidato[i].get_NM_UE() << endl;

            candidato[i].set_NR_VOTOS(0);

            candidato[i].set_NM_UE(nesima_palavra(1, linha));

            candidato[i].set_CD_CARGO(nesima_palavra(2, linha));
        
            candidato[i].set_DS_CARGO(nesima_palavra(3, linha));
  
            candidato[i].set_NR_CANDIDATO(nesima_palavra(4, linha));

            candidato[i].set_NM_CANDIDATO(nesima_palavra(5, linha));

            candidato[i].set_NM_URNA_CANDIDATO(nesima_palavra(6, linha));

            candidato[i].set_CD_SITUACAO_CANDIDATURA(nesima_palavra(7, linha));

            candidato[i].set_NR_PARTIDO(nesima_palavra(8, linha));

            candidato[i].set_SG_PARTIDO(nesima_palavra(9, linha));

            candidato[i].set_NM_PARTIDO(nesima_palavra(10, linha));

        }

        free(linha);
    }

    fclose(f);

    /* ####################################
        Aquisiçãos dos dados distritais
    #####################################*/

    FILE *p = fopen("data/consulta_cand_2018_DF.csv", "r");

    for(i = 25; i < 1263; i++){

         //cout << i << endl;

        a = ftell(p);

        while((c = fgetc(p)) != '\n');

        b = ftell(p);

        tamanho = b-a;

        linha = (char *) malloc(tamanho*sizeof(char));
        
        fseek(p, a, SEEK_SET);

        fgets(linha, tamanho, p);

        fseek(p, b, SEEK_SET);

        if(i != 25){
            // preencher_candidato(&(candidato[i]), linha);

            //cout << candidato[i].get_NM_UE() << endl;

            candidato[i].set_NR_VOTOS(0);

            candidato[i].set_NM_UE(nesima_palavra(1, linha));

            candidato[i].set_CD_CARGO(nesima_palavra(2, linha));
        
            candidato[i].set_DS_CARGO(nesima_palavra(3, linha));

            candidato[i].set_NR_CANDIDATO(nesima_palavra(4, linha));

            candidato[i].set_NM_CANDIDATO(nesima_palavra(5, linha));

            candidato[i].set_NM_URNA_CANDIDATO(nesima_palavra(6, linha));

            candidato[i].set_CD_SITUACAO_CANDIDATURA(nesima_palavra(7, linha));

            candidato[i].set_NR_PARTIDO(nesima_palavra(8, linha));

            candidato[i].set_SG_PARTIDO(nesima_palavra(9, linha));

            candidato[i].set_NM_PARTIDO(nesima_palavra(10, linha));
        }

        free(linha);

    }

    fclose(p);

        /* ####################################
        Aquisiçãos dos votos
    #####################################*/

    do{
        cout << "Insira o numero de eleitores: " ;

        cin >> numero_eleitores;

        if(numero_eleitores <= 0){

            cout << "Insira um numero de eleitores válido(>0): " ;

            cin >> numero_eleitores;

        }
    }while(numero_eleitores < 0);

        getchar();

    for(i=0; i<numero_eleitores; i++){

        // NOME ELEITOR

        eleitores.push_back(eleitor);
        
        do{
            cout << "Insira seu nome: ";

            getline(cin, nome);

            eleitores[i].set_NOME(nome);

            do{
                cout << "Seu nome é: " << eleitores[i].get_NOME() << " [s/n]" << endl;

                getline(cin, confirmacao);
                //getchar();
                confirmacao = upper(confirmacao);
            }while(confirmacao != "S" && confirmacao != "N");

        }while(confirmacao != "S");


        // DEPUTADO FEDERAL
        
        do{
            confirmacao = "";

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nDeputado Federal (4 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "6"){

                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }

                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                }

            }else{
                cout << "Voto Branco" << endl;
            }

            if(!inelegivel){

                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_DEPUTADO_FEDERAL(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_DEPUTADO_FEDERAL("NULO");
                    votos_nulos++;
                }

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_DEPUTADO_FEDERAL(candidato[indice_candidato].get_NM_CANDIDATO());
                }
        
            }

        }while(confirmacao != "CONFIRMAR");


        // DEPUTADO DISTRITAL

        do{
            confirmacao = "";

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nDeputado Distrital (5 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "8"){

                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }
            
                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                }
            
            }else{
                cout << "Voto Branco" << endl;
            }

            if(!inelegivel){
             
                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_DEPUTADO_DISTRITAL(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_DEPUTADO_DISTRITAL("NULO");
                    votos_nulos++;
                } 

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_DEPUTADO_DISTRITAL(candidato[indice_candidato].get_NM_CANDIDATO());
                }                               

            }
        
        }while(confirmacao != "CONFIRMAR");


        // SENADOR 1ª VAGA

        do{
            confirmacao = "";

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nSenador 1ª Vaga (3 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "5"){

                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "9"){
                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            cout << "\nPrimeiro Suplente\n" << endl;
                            candidato[j].print_urna();
                        }
                    }
                }

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "10"){
                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            cout << "\nSegundo Suplente\n" << endl;
                            candidato[j].print_urna();
                        }
                    }
                }
                
                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                }

            }else{
                cout << "Voto Branco" << endl;
            }


            if(!inelegivel){
             
                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_SENADOR_1(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_SENADOR_1("NULO");
                    votos_nulos++;
                }     

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_SENADOR_1(candidato[indice_candidato].get_NM_CANDIDATO());
                }

            }
        
        }while(confirmacao != "CONFIRMAR");


        // SENADOR 2ª VAGA

        do{
            confirmacao = "";

            voto_senador_repetido = false;

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nSenador 2ª Vaga (3 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){

                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "5" ){

                        if(candidato[j].get_NM_CANDIDATO() == eleitores[i].get_VOTO_SENADOR_1()){

                            voto_senador_repetido = true;
                            cout << "\nMesmo candidato para Senador 1ª vaga\n" << endl;
                        }

                        else if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }

                if(!voto_senador_repetido){

                    for(j = 0; j<1263; j++){
                        if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "9"){
                            if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                                cout << "\nPrimeiro Suplente\n" << endl;
                                candidato[j].print_urna();
                            }
                        }
                    }

                    for(j = 0; j<1263; j++){
                        if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "10"){
                            if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                                cout << "\nSegundo Suplente\n" << endl;
                                candidato[j].print_urna();
                            }
                        }
                    }

                }

                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                }

            }else{
                cout << "Voto Branco" << endl;
            }

            if(!inelegivel && !voto_senador_repetido){
             
                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_SENADOR_2(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_SENADOR_2("NULO");
                    votos_nulos++;
                }     

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_SENADOR_2(candidato[indice_candidato].get_NM_CANDIDATO());
                }

            }
        
        }while(confirmacao != "CONFIRMAR");

        // GOVERNADOR

        do{
            confirmacao = "";

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nGovernador (2 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){

                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "3"){

                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "4"){
                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            cout << "\nVice-Governador\n" << endl;
                            candidato[j].print_urna();
                        }
                    }
                }

                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                }            

            }else{
                cout << "Voto Branco" << endl;
            }

            if(!inelegivel){
             
                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_GOVERNADOR(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_GOVERNADOR("NULO");
                    votos_nulos++;
                }     

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_GOVERNADOR(candidato[indice_candidato].get_NM_CANDIDATO());
                }

            }
        
        }while(confirmacao != "CONFIRMAR");   

        // PRESIDENTE     

        do{
            confirmacao = "";

            inelegivel = false;

            voto_valido = false;

            indice_candidato = -1;

            cout << "\nPresidente (2 dígitos): \nDigite \"Branco\" para votar em branco" << endl;

            getline(cin, voto);

            voto = upper(voto);

            //getchar();

            if(voto != "BRANCO"){

                for(j = 0; j<1263; j++){

                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "1"){

                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            candidato[j].print_urna();
                            voto_valido = true;
                            indice_candidato = j;
                        }else{
                            cout << "Candidato inelegível" << endl;
                            inelegivel = true;
                        }
                    }
                }

                for(j = 0; j<1263; j++){
                    if(voto == candidato[j].get_NR_CANDIDATO() && candidato[j].get_CD_CARGO() == "2"){
                        if(candidato[j].get_CD_SITUACAO_CANDIDATURA() != "3"){
                            cout << "\nVice-Presidente\n" << endl;
                            candidato[j].print_urna();
                        }
                    }
                }

                if(!voto_valido){
                    cout << "Voto nulo" << endl;
                } 

            }else{
                cout << "Voto Branco" << endl;
            }

            if(!inelegivel){
             
                do{

                    cout << "Digite \"Confirmar\" e pressione Enter para confirmar, ou \"Corrigir\" para corrigir: ";

                    getline(cin, confirmacao);

                    confirmacao = upper(confirmacao);

                    //getchar();
                
                }while(confirmacao != "CONFIRMAR" && confirmacao != "CORRIGIR");

                if(voto == "BRANCO" && confirmacao == "CONFIRMAR"){
                    eleitores[i].set_VOTO_PRESIDENTE(voto);
                    votos_brancos++;
                }

                else if(confirmacao == "CONFIRMAR" && !voto_valido){
                    eleitores[i].set_VOTO_PRESIDENTE("NULO");
                    votos_nulos++;
                }     

                else if(confirmacao == "CONFIRMAR" && voto_valido){
                    candidato[indice_candidato].contar_voto();
                    eleitores[i].set_VOTO_PRESIDENTE(candidato[indice_candidato].get_NM_CANDIDATO());
                }

            }
        
        }while(confirmacao != "CONFIRMAR"); 

    }

    cout << "\n\nResultados";

    for(i=0; i<numero_eleitores; i++){

        eleitores[i].print_ELEITOR();
        
    }

    cout << "\nDeputados Federais:\n\n";

    for(i=0; i<1263; i++){
        if(candidato[i].get_NR_VOTOS() > 0 && candidato[i].get_CD_CARGO() == "6"){
            cout << candidato[i].get_NM_CANDIDATO() << ": " << candidato[i].get_NR_VOTOS() << " Voto(s)" << endl;
        }
    }

    cout << "\nDeputados Distritais:\n\n";

    for(i=0; i<1263; i++){
        if(candidato[i].get_NR_VOTOS() > 0 && candidato[i].get_CD_CARGO() == "8"){
            cout << candidato[i].get_NM_CANDIDATO() << ": " << candidato[i].get_NR_VOTOS() << " Voto(s)" << endl;
        }
    }

    cout << "\nSenadores:\n\n";

    for(i=0; i<1263; i++){
        if(candidato[i].get_NR_VOTOS() > 0 && candidato[i].get_CD_CARGO() == "5"){
            cout << candidato[i].get_NM_CANDIDATO() << ": " << candidato[i].get_NR_VOTOS() << " Voto(s)" << endl;
        }
    }

    cout << "\nGovernadores:\n\n";

    for(i=0; i<1263; i++){
        if(candidato[i].get_NR_VOTOS() > 0 && candidato[i].get_CD_CARGO() == "3"){
            cout << candidato[i].get_NM_CANDIDATO() << ": " << candidato[i].get_NR_VOTOS() << " Voto(s)" << endl;
        }
    }

    cout << "\nPresidentes:\n\n";

    for(i=0; i<1263; i++){
        if(candidato[i].get_NR_VOTOS() > 0 && candidato[i].get_CD_CARGO() == "1"){
            cout << candidato[i].get_NM_CANDIDATO() << ": " << candidato[i].get_NR_VOTOS() << " Voto(s)" << endl;
        }
    }

    cout << "\nVotos Nulos: " << votos_nulos << endl;
    cout << "Votos Brancos: " << votos_brancos << endl;
    
    return 0;
}