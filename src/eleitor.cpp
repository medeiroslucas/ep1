#include<bits/stdc++.h>

#include"eleitor.hpp"

using namespace std;

Eleitor::Eleitor(){

    NOME = "";
    VOTO_DEPUTADO_FEDERAL = "";
    VOTO_DEPUTADO_DISTRITAL = "";
    VOTO_SENADOR_1 = "";
    VOTO_SENADOR_2 = "";
    VOTO_GOVERNADOR = "";
    VOTO_PRESIDENTE = "";

}

Eleitor::~Eleitor(){
}

string Eleitor::get_NOME(){
    return NOME;
}
void Eleitor::set_NOME(string NOME){
    this->NOME = NOME;
}

string Eleitor::get_VOTO_DEPUTADO_FEDERAL(){
    return VOTO_DEPUTADO_FEDERAL;
}
void Eleitor::set_VOTO_DEPUTADO_FEDERAL(string VOTO_DEPUTADO_FEDERAL){
    this->VOTO_DEPUTADO_FEDERAL = VOTO_DEPUTADO_FEDERAL;
}

string Eleitor::get_VOTO_DEPUTADO_DISTRITAL(){
    return VOTO_DEPUTADO_DISTRITAL;
}
void Eleitor::set_VOTO_DEPUTADO_DISTRITAL(string VOTO_DEPUTADO_DISTRITAL){
    this->VOTO_DEPUTADO_DISTRITAL = VOTO_DEPUTADO_DISTRITAL;
}

string Eleitor::get_VOTO_SENADOR_1(){
    return VOTO_SENADOR_1;
}
void Eleitor::set_VOTO_SENADOR_1(string VOTO_SENADOR_1){
    this->VOTO_SENADOR_1 = VOTO_SENADOR_1;
}

string Eleitor::get_VOTO_SENADOR_2(){
    return VOTO_SENADOR_2;
}
void Eleitor::set_VOTO_SENADOR_2(string VOTO_SENADOR_2){
    this->VOTO_SENADOR_2 = VOTO_SENADOR_2;
}

string Eleitor::get_VOTO_GOVERNADOR(){
    return VOTO_GOVERNADOR;
}
void Eleitor::set_VOTO_GOVERNADOR(string VOTO_GOVERNADOR){
    this->VOTO_GOVERNADOR = VOTO_GOVERNADOR;
}

string Eleitor::get_VOTO_PRESIDENTE(){
    return VOTO_PRESIDENTE;
}
void Eleitor::set_VOTO_PRESIDENTE(string VOTO_PRESIDENTE){
    this->VOTO_PRESIDENTE = VOTO_PRESIDENTE;
}

void Eleitor::print_ELEITOR(){

    cout << "\n" << endl;
    cout << NOME << endl;
    cout << "Deputado Federal: " << VOTO_DEPUTADO_FEDERAL << endl;
    cout << "Deputado Distrital: " << VOTO_DEPUTADO_DISTRITAL << endl;
    cout << "Senador 1ª vaga: " << VOTO_SENADOR_1 << endl;
    cout << "Senador 2ª vaga: " << VOTO_SENADOR_2 << endl;
    cout << "Governador: " << VOTO_GOVERNADOR << endl;
    cout << "Presidente: " << VOTO_PRESIDENTE << endl;

}